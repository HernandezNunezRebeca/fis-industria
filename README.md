## Industria 4.0
```plantuml
@startmindmap
+[#F3BCDD] Industria 4.0
**_ Es
+++[#BCF3F1] La Cuarta Revolución industrial tambien es conocida como Industria 4.0, \n está cambiando la manera en que los negocios operan y asi mismo \n cambia los mercados con los que se ven obligados a competir.
++++[#F3BCDD] Involucra tecnología.
*****_ Como lo son
++++++[#BCF3F1] Cloud computing.
++++++[#BCF3F1] Robotica.
++++++[#BCF3F1] Big data.
++++++[#BCF3F1] Inteligencia artificial.
++++++[#BCF3F1] Sistema autónomo.
++++++[#BCF3F1] lot.
++++[#F3BCDD] El proposito que se tiene, es que en un futuro \n haya menos inversion humana en el trabajo. 
++++[#F3BCDD] Tiene muchas cosas nuevas.
*****_ como es
++++++[#BCF3F1] Nuevos trabajos.
*******_ como
++++++++[#F3BCDD]  Desarrollador de software.
++++++++[#F3BCDD]  Youtubers
++++++++[#F3BCDD]  Gamers
++++[#F3BCDD] Se generan grandes cambios.
*****_ como
+++++[#BCF3F1] La integración de TICs en la industria de mano factura y servicios. 
******_ por ejemplo
+++++++[#F3BCDD] Los alamacenes que son ordenados por robots como en Amazon. 
********_ tiene efectos
+++++++++[#BCF3F1] La reduccion de puestos de trabajo \n y aparicion de nuvas profesiones.
++++++[#F3BCDD] Las empresas de manofactura de vulven empresas TICs.
*******_ por ejemplo
++++++++[#BCF3F1] Los carros modernos que se ofrecen actualmente al publico terminan siendo computadoras.
+++++++[#BCF3F1] Nuevos paradicmas y tecnologías.
********_ por ejemplo
+++++++++[#F3BCDD] Las empresas que no se adaptan a los cambios rapidos, estan destinados a desaparecer.
**********_ como lo son
+++++++++++[#BCF3F1] Nokia \n Blockbuster \n Entre otras.
++++++++++++[#F3BCDD] Al no tomarce la importancia adecuada al avance tecnologíco,\n estas empresas fueron desapareciendo poco a poco.
++++[#BCF3F1] Es importante en la actualidad.
*****_ ya que 
++++++[#F3BCDD] La tecnología nos facilita la forma de vivir.
++++++[#F3BCDD] Se crean nuevos trabajos por lo que se nos dan\n mas oportunidades.
++++++[#F3BCDD] Usamos la tecnología de manera habitual en nuetra vida diaria.
++++[#BCF3F1] Nos regala nueva tecnología.
*****_ como lo es
++++++[#F3BCDD] Los taxis que se manejan de manera autonoma.
++++++[#F3BCDD] La realidad aumentada.
++++++[#F3BCDD] Comunicaciones más veloces. 
@endmindmap
```


## México y la Industria 4.0
```plantuml
@startmindmap
+[#ED7A68] México y la Industria 4.0
**_ .
+++[#68ED83] México a progresado gracias a la industria 4.0
****_ por ejemplo 
+++++[#ED7A68] La empresa sin llaves que se esta encargando de realizar \n una aplicación con la cual no será necesario seguir ocupando llaves,\n  si no podemos ocupar nustros dispositivos moviles.
+++++[#ED7A68] El Ing Ernesto Rodriguez \n Creo un Exoesqueleto que ayuda \n a recuperar la movilidad perdida en las persona. 
+++[#68ED83] Existen empresas en México que cuentan con equipos antiguos y modernos, \n por lo que es un gran opstaculo para nuestro pais. 
****_ .
+++++[#ED7A68] Se necesita apoyar al pais para que pueda segir creciendo \n y adaptarse a la nueva tecnologia. 
+++[#68ED83] Algunas empresas extrangeras comenzaron a hacer uso de la inteligencia \n artificial, pero en México tambien se esta comenzando a usar \n para incrementar la eficiencia. 
+++[#68ED83] México necesita nuevos recursos. 
****_ como
+++++[#ED7A68] Mas modernidad en los equipos para que se pueda trabajar \n de una mamnera mas eficiente. 
+++++[#ED7A68] Apoyo a los Ingenieros que se dedican a hacer estudio de las tecnologías.
+++++[#ED7A68] Mejores espacios de trabajo.
+++++[#ED7A68] De incrementar el apoyo, se pordria crear nuevas oportunidades \n y asi México creceria de una mejor manera.
@endmindmap
```
